import logging

from aiogram import executor

from bot import dp

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    executor.start_polling(dp)
