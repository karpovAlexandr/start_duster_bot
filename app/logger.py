import logging
import os
from logging.handlers import TimedRotatingFileHandler

log_folder = "logs"


class LevelFileHandler(TimedRotatingFileHandler):
    level_map = {
        50: "CRITICAL",
        40: "ERROR",
        30: "WARNING",
        20: "INFO",
        10: "DEBUG",
        0: "NOTSET",
    }
    filename_ext = ".log"

    def __init__(
        self, filename: str = f"{log_folder}/INFO.log", mode: str = "a", *args, **kwargs
    ) -> None:
        self.mode = mode
        self.filename = filename
        super().__init__(filename, *args, **kwargs)
        if not os.path.exists(f"{log_folder}"):
            os.makedirs(log_folder)

    def emit(self, record: logging.LogRecord) -> None:
        self.filename: str = self.level_map[record.levelno] + self.filename_ext
        self.stream = open(f"{log_folder}/{self.filename}", mode=self.mode)
        super().emit(record)
