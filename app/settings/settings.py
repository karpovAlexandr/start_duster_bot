import os
from logging import config
from os import getcwd
from pathlib import Path

from pydantic import BaseSettings


class Settings(BaseSettings):
    TOKEN: str

    EMOJI_TRIGGERS: list[str]
    SANDWICH_TRIGGERS: list[str]
    FAG_TRIGGERS: list[str]
    OK_TRIGGERS: list[str]
    FU_TRIGGERS: list[str]

    EXCLUDE_USERS: list[int]

    class Config:
        env_file = ".env"


def init_settings() -> "Settings":
    working_dir: str = getcwd()
    settings_dir: str = str(Path(__file__).parent.absolute())
    if working_dir != settings_dir:
        os.chdir(settings_dir)
    settings: "Settings" = Settings()
    os.chdir(working_dir)
    return settings


FORMAT: str = "%(levelname)s | %(name)s | " "%(asctime)s | %(lineno)d | %(message)s"
LOGGER_LEVEL: int = int(os.environ.get("LOGGER_LEVEL", 20))

dict_config: dict = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"base": {"format": FORMAT}},
    "handlers": {
        "file": {
            "class": "logger.LevelFileHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
            "when": "d",
            "interval": 1,
        },
        "stream": {
            "class": "logging.StreamHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
        },
    },
    "loggers": {
        "": {
            "level": LOGGER_LEVEL,
            "handlers": ["file", "stream"],
            "propagate": False,
        }
    },
}

config.dictConfig(dict_config)
settings: Settings = init_settings()
