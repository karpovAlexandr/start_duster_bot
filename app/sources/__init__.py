from os.path import join
from pathlib import Path

VIDEOS_FOLDER_NAME: str = "videos"
PICTURE_FOLDER_NAME: str = "pics"

videos_folder: str = join(str(Path(__file__).parent.absolute()), VIDEOS_FOLDER_NAME)
picture_folder: str = join(str(Path(__file__).parent.absolute()), PICTURE_FOLDER_NAME)

__all__ = [
    "videos_folder",
    "picture_folder",
]
