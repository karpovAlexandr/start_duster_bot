import logging
from re import findall

from fuzzywuzzy import fuzz

MATCH_RATE: int = 70

logger = logging.getLogger(__name__)


def tokenize(text: str) -> list[str]:
    return findall(r"[^\w\s]+|\w+", text)


# https://sysblok.ru/knowhow/kak-nahodit-pohozhie-slova-s-pomoshhju-rasstojanija-levenshtejna/
def find_variation(text: str, standard: str) -> bool:
    tokenized_string: list[str] = [token.lower() for token in tokenize(text)]
    # TODO it's testing now, replace with any()
    for word in tokenized_string:
        rate = fuzz.ratio(standard, word)
        logger.info(f"Got {rate=}: for '{word}'")
        if rate > MATCH_RATE:
            return True
    return False
