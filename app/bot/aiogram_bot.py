import logging

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext

from bot.triggers import triggers_list, fag_trigger, fu_trigger
from bot.utils import send_video, fag_answer, fu_answer
from settings import settings

bot: "Bot" = Bot(token=settings.TOKEN)
dp: "Dispatcher" = Dispatcher(bot, storage=MemoryStorage())

logger = logging.getLogger(__name__)

TIMEOUT: int = 60


@dp.message_handler(
    lambda msg: True
)
async def you_are_welcome(message: types.Message, state: FSMContext) -> None:
    if any(trigger(message, settings) for trigger in triggers_list):
        await state.finish()
        return await send_video(bot, message)

    if fag_trigger(message, settings):
        await state.finish()
        return await fag_answer(bot, message)

    if fu_trigger(message, settings):
        await state.finish()
        return await fu_answer(bot, message)
