import logging

import emoji
from aiogram.types import Message

from bot.handlers import find_variation
from settings import Settings

logger = logging.getLogger(__name__)


def sandwich_trigger(message: Message, settings: Settings) -> bool:
    triggers: list[str] = settings.SANDWICH_TRIGGERS
    text: str = message.text
    return any(find_variation(text, trigger) for trigger in triggers)


def clown_sticker_trigger(message: Message, settings: Settings) -> bool:
    trigger_list: list[str] = settings.EMOJI_TRIGGERS
    emoji_obj: str = emoji.demojize(message.text)
    return any(trigger in emoji_obj for trigger in trigger_list)


def fag_trigger(message: Message, settings: Settings) -> bool:
    triggers: list[str] = settings.FAG_TRIGGERS
    text: str = message.text
    return any(find_variation(text, trigger) for trigger in triggers)


def fu_trigger(message: Message, settings: Settings) -> bool:
    triggers: list[str] = settings.FU_TRIGGERS
    text: str = message.text
    return any(find_variation(text, trigger) for trigger in triggers)


triggers_list = [
    sandwich_trigger,
    clown_sticker_trigger,
]
