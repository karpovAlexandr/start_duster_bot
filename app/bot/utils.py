from os import listdir, path
from random import choice

from aiogram import Bot, types

from sources import videos_folder


def get_video_path(_videos_folder: str = videos_folder) -> str:
    video: str = choice(
        [video for video in listdir(_videos_folder) if video.endswith(".mp4")]
    )
    return path.join(_videos_folder, video)


def fag_answer(bot: Bot, message: types.Message):
    return bot.send_message(
        message.chat.id,
        "а может ты пидор?",
        reply_to_message_id=message.message_id,
    )


def fu_answer(bot: Bot, message: types.Message):
    return bot.send_message(
        message.chat.id,
        "а может сам пойдешь нахуй?",
        reply_to_message_id=message.message_id,
    )


async def send_video(bot: Bot, message: types.Message):
    video_path: str = get_video_path()
    await bot.send_video(
        message.chat.id,
        open(video_path, "rb"),
        reply_to_message_id=message.message_id,
    )
